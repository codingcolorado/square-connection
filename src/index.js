import Axios from 'axios';

class SquareConnection {
  constructor(accessToken = '', version = 'v2', connectUrl = 'https://connect.squareup.com/') {
    if (!accessToken) throw new Error('You must specify your access token in the constructor of the SquareConnection module');
    
    this.accessToken = accessToken;
    this.version = version;
    this.connectUrl = connectUrl;
    this.baseURL = connectUrl + version;
  }

  api(method = 'GET', path = '', body = {}) {
    if (!['GET', 'POST', 'PUT', 'DELETE'].includes(method)) throw new Error('Invalid request method sent to API request');
    if (!path) throw new Error('You must specify a path in your API request');

    return Axios.request({
      method: method,
      url: this.baseURL + path,
      data: body,
      headers: {
        'Authorization': 'Bearer ' + this.accessToken
      }
    });
  }

  get(path = '', query = {}) {
    if (!path) throw new Error('You must specify a path in your GET request');

    path += SquareConnection.queryString(query);
    return this.api('GET', path);
  }

  post(path = '', body = {}) {
    if (!path) throw new Error('You must specify a path in your POST request');

    return this.api('POST', path, body)
  }

  put(path = '', body = {}) {
    if (!path) throw new Error('You must specify a path in your PUT request');

    return this.api('PUT', path, body);
  }

  delete(path = '', query = {}) {
    if (!path) throw new Error('You must specify a path in your DELETE request');

    path += SquareConnection.queryString(query);
    return this.api('DELETE', path);
  }

  static queryString(query = {}) {
    let queryString = Object.keys(query).map((key) => {
      return key + '=' + encodeURIComponent(obj[key]);
    }).join('&');

    if (!queryString) return '';
    return '?' + queryString;
  }
}

export default SquareConnection;