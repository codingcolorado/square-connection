# Square Connection
A simple way to connect to the Square Connect API.

## What this Is
SquareConnection is a promise-based module built with Axios that offers a quick and easy way to start hitting the Square Connect API.

## Getting Started

`npm install --save square-connection`

## Simple Usage

Import the module:

`import SquareConnection from 'square-connection';`

Use the module:

```
const square = new SquareConnection('YOUR_ACCESS_TOKEN');
const request = square.get('/customers');

request.then((result) => {
  console.log(result); // logs out an Axios result object if the API call succeded
});

request.catch((error) => {
  console.log(error); // logs out an Axios error object if the API call didn't succed
});

```

## Constructor

The constructor takes in three parameters: the accessToken (required), the connect version (v2), and the connect url.

Param       | Type    | Default                           | Description
----------- | ------- | --------------------------------- | -----------------------------------------------------------------------------------------------
accessToken | String  | `''` (Required)                   | The access token from your [Square Application](https://connect.squareup.com/apps)
version     | String  | `'v2'`                            | The Square Connect API version (v2 is the only supported version, but you're welcome to try v1)
connectUrl  | String  | `'https://connect.squareup.com/'` | The url through which to connect to the Square Connect API (should probably be left alone)

## API

Currently, you there are no specific methods in this module's API, though they are set to come soon. As for now, it serves as only a quick way of doing GET, POST, PUT, and DELETE requests to Square. You define the route and the body.

Method  | Parameters                | Description
------- | ------------------------- | ----------------------------------
get     | String path, Object query | The path defines the resource being requested. The query should be a single-level JS object with key-value pairs which can/will be encoded via [encodeURIComponent](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent)
post    | String path, Object body  | The path defines the resource being posted to. The body is a simple JS object to be accepted by Square's API
put     | String path, Object body  | The path defines the resource being put to. The body is a simple JS object to be accepted by Square's API
delete  | String path, Object query | The path defines the resource being deleted. The query should be a single-level JS object with key-value pairs which can/will be encoded via [encodeURIComponent](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent)

## Example

This example comes straight from [Square's REST API Reference](https://docs.connect.squareup.com/api/connect/v2/#endpoint-charge)

```
// charge a customer's card

import SquareConnection from 'square-connection';

const square = new SquareConnection('YOUR_ACCESS_TOKEN');
const mainLocation = 'YOUR_LOCATION_ID';

const body = {
  "idempotency_key": "74ae1696-b1e3-4328-af6d-f1e04d947a13",
  "shipping_address": {
    "address_line_1": "123 Main St",
    "locality": "San Francisco",
    "administrative_district_level_1": "CA",
    "postal_code": "94114",
    "country": "US"
  },
  "billing_address": {
    "address_line_1": "500 Electric Ave",
    "address_line_2": "Suite 600",
    "administrative_district_level_1": "NY",
    "locality": "New York",
    "postal_code": "10003",
    "country": "US"
  },
  "amount_money": {
    "amount": 5000,
    "currency": "USD"
  },
  "card_nonce": "SOME_CARD_NONCE",
  "reference_id": "some optional reference id",
  "note": "some optional note",
  "delay_capture": false
}

const request = square.post('/locations/' + mainLocation + '/transactions', body);

request.then((result) => {
  console.log(result);
});

request.catch((error) => {
  console.log(error);
});

```


## More Information

The connection to the Square API is done via [Axios](https://www.npmjs.com/package/axios).

If you see something you think is missing or would like to contribute, please check out the [Bitbucket repo](https://bitbucket.org/codingcolorado/square-connection/)

